package com.example.lab_1;

import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class TransactionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        Bundle extra = getIntent().getBundleExtra("transactions");
        ArrayList<Transaction> transactions = (ArrayList<Transaction>) extra.getSerializable("transactions");


        final TableLayout table = findViewById(R.id.incoming_trans);

        for (int i = transactions.size() - 1; i >= 0; i--) {
            TableRow row = new TableRow(this);
            String name = transactions.get(i).name;
            String time = transactions.get(i).time;
            String amount = Integer.toString(transactions.get(i).amount);
            String balance = Integer.toString(transactions.get(i).balance);

            TextView tv1 = new TextView(this);
            tv1.setText(time);
            tv1.setTextSize(20);

            TextView tv2 = new TextView(this);
            tv2.setText(name);
            tv2.setTextSize(20);

            TextView tv3 = new TextView(this);
            tv3.setText(amount);
            tv3.setTextSize(20);
            if (transactions.get(i).toMe) {
                tv3.setTextColor(Color.parseColor("#5FB404"));
            } else {
                tv3.setTextColor(Color.parseColor("#FF0000"));
            }

            TextView tv4 = new TextView(this);
            tv4.setText(balance);
            tv4.setTextSize(20);

            row.addView(tv1);
            row.addView(tv2);
            row.addView(tv3);
            row.addView(tv4);

            row.setClickable(true);
            row.setLongClickable(true);
            row.setBackgroundColor(0x00000000);

            table.addView(row);
        }


        for (int i = 1; i < table.getChildCount(); i++) {
            final TableRow row = (TableRow) table.getChildAt(i);
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ColorDrawable drawable = (ColorDrawable) row.getBackground();
                    if (drawable.getColor() != Color.rgb(176, 120, 52)) {
                        row.setBackgroundColor(Color.rgb(176, 120, 52));
                    } else {
                        row.setBackgroundColor(0x00000000);
                    }
                }
            });
            row.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    TextView nameView = (TextView) row.getChildAt(1);
                    TextView amountView = (TextView) row.getChildAt(2);

                    final String name = nameView.getText().toString();
                    final String amount = amountView.getText().toString();
                    Toast toast = Toast.makeText(TransactionsActivity.this, name + "  " + amount, Toast.LENGTH_LONG);
                    toast.show();
                    return true;
                }
            });
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_main);
        }
    }
}

package com.example.lab_1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    public static final int MINIMUMBALANCE = 90;
    public static final int REQUEST_CODE = 1;
    static int balance = new Random().nextInt(21) + MINIMUMBALANCE;
    static final List<Transaction> transactions = new ArrayList<>();
    final String[] recipientList = {"Alice", "Bob", "Charlie", "Marius", "Dawn", "Elvis", "Frode"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Add the initial transaction
        if(transactions.size() == 0) { //If first transaction.
            transactions.add(new Transaction(true, getTime(), balance, balance, "Angel"));
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Make text view and set it to the current balance
        final TextView balanceView = findViewById(R.id.lbl_balance);
        balanceView.setText(Integer.toString(balance));

        //Make button views
        Button transferButton = findViewById(R.id.btn_transfer);
        Button transactionsButton = findViewById(R.id.btn_transactions);

        //On click listener for the transfer button
        transferButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TransferActivity.class);
                intent.putExtra("recipients", recipientList);   //Send the programmatically populated recipient list
                intent.putExtra("balance", balance);            //Send the balance for amount_check
                startActivityForResult(intent, REQUEST_CODE);          //Start activity and expect a result
            }
        });

        transactionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TransactionsActivity.class);
                Bundle extra = new Bundle();
                extra.putSerializable("transactions", (Serializable) transactions); //Send the list of transactions
                intent.putExtra("transactions", extra);
                startActivity(intent);          //Start activity
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                transactions.add(new Transaction(false, getTime(), Integer.parseInt(data.getStringExtra("amount")),
                        balance -= Integer.parseInt(data.getStringExtra("amount")), data.getStringExtra("recipient")));
            }
            final TextView balanceView = findViewById(R.id.lbl_balance);
            balanceView.setText(Integer.toString(balance));
        }

    }


    //Returns the current time in hh:mm:ss
    public String getTime() {
        Calendar calendar = Calendar.getInstance();
        return (new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(calendar.getTime()));
    }


}



package com.example.lab_1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


public class TransferActivity extends AppCompatActivity {

    final String INSUFFICIENT_FUNDS = "INSUFFICIENT FUNDS";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        //Get recipients from main activity
        Intent intent = this.getIntent();
        final String[] recipientList = intent.getStringArrayExtra("recipients");
        final int balance = intent.getIntExtra("balance", 0);

        //Make the views
        final Spinner recipients = findViewById(R.id.recipient_spinner);
        final Button payButton = findViewById(R.id.btn_pay);
        final EditText amountEdit = findViewById(R.id.txt_amount);


        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_spinner_item,
                recipientList
        );
        recipients.setAdapter(adapter);


        payButton.setAlpha((float)0.5);
        payButton.setBackgroundColor(Color.GRAY);
        payButton.setEnabled(false); //Disable pay button by default




        //Listener to follow up changes in the amountEdit field
        amountEdit.addTextChangedListener(new TextWatcher() {
            @Override       //Befor text changes, set buttons background to gray
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                payButton.setBackgroundColor(Color.GRAY);
            }

            @Override      //Set what to be done when text changes
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(amountEdit.length() > 0 && Integer.parseInt(amountEdit.getText().toString()) > 0) {               //If there is a value, enable pay button
                    payButton.setBackgroundColor(Color.GREEN);
                    payButton.setEnabled(true);
                } else {                                    //If blank, disable pay button
                    payButton.setBackgroundColor(Color.GRAY);
                    payButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //Click listener for pay button
        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        //Check if transaction can be done, error message if insufficient funds
                if(Integer.parseInt(amountEdit .getText().toString()) > balance) {
                    final TextView error = findViewById(R.id.lbl_amount_check);
                    error.setText(INSUFFICIENT_FUNDS);
                } else {    //If sufficient funds, go through with the transaction
                    Intent intent = new Intent();
                    intent.putExtra("recipient", recipients.getSelectedItem().toString());
                    intent.putExtra("amount", amountEdit.getText().toString());
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });


    }



}

package com.example.lab_1;

import java.io.Serializable;

 public class Transaction implements Serializable {
    Boolean toMe;
    String time;
    Integer amount;
    Integer balance;
    String name;

    /*public Transaction() {
        toMe = false;
        time = "00:00:00";
        amount = 0;
        balance = 0;
        name = "";
    } */

    public Transaction(Boolean sToMe, String sTime, Integer sAmount, Integer sBalance, String sName) {
        toMe = sToMe;
        time = sTime;
        amount = sAmount;
        balance = sBalance;
        name = sName;
    }
}
